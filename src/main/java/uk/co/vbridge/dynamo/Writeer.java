package uk.co.vbridge.dynamo;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class Writeer {


    public static void main(String[] args) {

        //for real aws dynam dob
        //AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();

        //for local dynamodb
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "eu-west-2"))
                .build();

        DynamoDBMapper mapper = new DynamoDBMapper(client);

        CatalogItem item = new CatalogItem();
        item.setId(102);
        item.setTitle("Book 102 Title");
        item.setISBN("222-2222222222");
        item.setSomeProp("Test");

        mapper.save(item);
    }
}
